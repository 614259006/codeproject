<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">

<body>
    <div class="container">
        <div class="card mt-3">
            <table class="table">

                <tr>
                    <th>Food id</th>
                    <th>Food Name</th>
                    <th>Food Price</th>
                    <th>Food Amount</th>
                </tr>
                <?php
                foreach ($menu->result_array() as $row) {
                ?>
                    <tr>

                        <td><?= $row['fid'] ?></td>
                        <td><?= $row['fname'] ?></td>
                        <td><?= $row['fprice'] ?></td>
                        <td><?= $row['famount'] ?></td>

                    </tr>


                <?php } ?>
            </table>
        </div>
    </div>
</body>

</html>